#include <iostream>
#include <fstream>
#include <typeinfo>

using namespace std;

//����� ������
class Str {
private:
	char* arr = nullptr;
	int size;
public:
	//����������� �� ���������
	Str() {
		arr = new char[1]{ '\0' };
		size = 0;
	}

	//����������� � ����������
	Str(const char* str) {
		int len = strlen(str);
		arr = new char[len + 1]; //���� ������� \0
		for (int i = 0; i < len; i++) {
			arr[i] = str[i];
		}
		size = len;
		arr[size] = '\0';
	}

	//����������� �����������
	Str(const Str& s) {
		*this = s; //�������� ������������
	}

	//�������� ������������
	Str& operator=(const Str& s) {
		if (this != &s) {
			size = s.size;
			arr = new char[size + 1]; //���� ������� \0
			for (int i = 0; i < size; i++) {
				arr[i] = s.arr[i];
			}
			arr[size] = '\0';
		}
		return *this;
	}

	//���������� ������ � �����
	friend ostream& operator<<(ostream& os, const Str& str) {
		if (str.arr) {
			os << str.arr;
		}
		return os;
	}

	friend istream& operator>>(istream& ins, Str& str) {
		char buf[255];
		ins.getline(buf, 255);
		int len = strlen(buf);
		str.arr = new char[len + 1];
		for (int i = 0; i < len; i++) {
			str.arr[i] = buf[i];
		}
		str.arr[len] = '\0';
		str.size = len;
		return ins;
	}

	//���������
	friend bool operator==(const Str& a, const Str& b) {
		if (a.size != b.size)
			return false;
		else {
			for (int i = 0; i < a.size; i++) {
				if (a.arr[i] != b.arr[i])
					return false;
			}
			return true;
		}
	}

	char* getPtr() {
		return arr;
	}

	void setPtr(char* ptr) {
		arr = ptr;
	}

	int getSize() {
		return size;
	}

	void setSize(int value) {
		size = value;
	}

	~Str() {
		if (size) {
			size = 0;
			delete[] arr;
		}
	}
};

class Person;

// ������
class Letter {
private:
	virtual void readLetter() = 0; //����� ����������� �����

protected:
	Str dest; //����������

public:
	Letter(Str dest) {
		this->dest = dest;
	}

	Str getDest() {
		return dest;
	}

	//����������� �����������
	Letter(Letter&& lt) {
		*this = move(lt);
	}

	//�������� �����������
	Letter& operator=(Letter&& lt) noexcept {
		if (this != &lt) {
			dest.~Str();
			dest.setSize(lt.dest.getSize());
			dest.setPtr(lt.dest.getPtr());
			lt.dest.setPtr(nullptr);
			lt.dest.setSize(0);
		}
		return *this;
	}

	friend bool operator==(const Letter& a, const Str& b) {
		return a.dest == b;
	}

	friend Person;
};

//�������� ������ (��������� ���������� �������� ������)
class Service : public Letter {
private:
	Str blank; //����� ������

	void readLetter() override {
		cout << "���������� �� ������: " << blank << endl;
		cout << "����������: " << dest << endl;
	}

public:
	Service(Str dest, Str blank) : Letter(dest) {
		this->blank = blank;
	}

	Service(Service&& s) : Letter(move(s)) {
		blank.~Str();
		blank.setSize(s.blank.getSize());
		blank.setPtr(s.blank.getPtr());
		s.blank.setPtr(nullptr);
		s.blank.setSize(0);
	}
	Service& operator=(Service&& s) noexcept {
		if (this != &s) {
			dest.~Str();
			dest.setSize(s.dest.getSize());
			dest.setPtr(s.dest.getPtr());
			s.dest.setPtr(nullptr);
			s.dest.setSize(0);
			blank.~Str();
			blank.setSize(s.blank.getSize());
			blank.setPtr(s.blank.getPtr());
			s.blank.setPtr(nullptr);
			s.blank.setSize(0);
		}
		return *this;
	}
};

//������ � �������� ������
class Money : public Letter {
private:
	Str text;
	double money;

	//��������������
	void readLetter() override {
		cout << "��������: " << text << endl;
		cout << "�����: " << money << endl;
		cout << "����������: " << dest << endl;
	}

public:
	Money(Str dest, Str text, double money) : Letter(dest) {
		this->text = text;
		this->money = money;
	}

	Money(Money&& m) : Letter(move(m)) {
		text.~Str();
		text.setSize(m.text.getSize());
		text.setPtr(m.text.getPtr());
		money = m.money;
		m.text.setPtr(nullptr);
		m.text.setSize(0);
		m.money = 0;
	}

	Money& operator=(Money&& m) noexcept {
		if (this != &m) {
			dest.~Str();
			dest.setSize(m.dest.getSize());
			dest.setPtr(m.dest.getPtr());
			m.dest.setPtr(nullptr);
			m.dest.setSize(0);
			text.~Str();
			text.setSize(m.text.getSize());
			text.setPtr(m.text.getPtr());
			money = m.money;
			m.text.setPtr(nullptr);
			m.text.setSize(0);
			m.money = 0;
		}
		return *this;
	}
};

class Person {
private:
	Str name;

public:
	Letter* lt = nullptr; //������

public:

	Person(Str name) {
		this->name = name;
		lt = nullptr;
	}

	Str getName() {
		return name;
	}

	Letter* writeLetter() {
		cout << "����� ������ �� ������ ���������:" << endl;
		cout << "1 - ��������;" << endl;
		cout << "2 - � �������� ������;" << endl;
		char ans;
		cin >> ans;
		Str str1;
		double money;
		if (ans == '1') {
			cout << "��������� �����: ";
			getchar();
			cin >> str1;
		}
		else {
			cout << "������� ������ �������� �����: ";
			cin >> money;
			getchar();
			cout << "��������: ";
			cin >> str1;
		}
		cout << "������� ����������: ";
		Str str2;
		cin >> str2;
		if (ans == '1') {
			return new Service(str2, str1);
		}
		else {
			return new Money(str2, str1, money);
		}
	}

	//�������� ������
	void readLetter() {
		if (lt) {
			lt->readLetter();
		}
		else {
			cout << "��� ������" << endl;
		}
	}

	//������ ������
	void setLetter(Letter* ptr) {
		lt = ptr;
	}

	Letter* getLetter() {
		return lt;
	}

	~Person() {
		if (lt)
			delete lt;
	}
};

//�����
template<typename t>
class Post {
private:
	t** data = nullptr;
	int capacity; //������� ����� ����� ����������
	int size; //������� ���-�� ����� �� �����

protected:

	//���� �� ������� ����� ��� ��������� �������
	void resize(int newcap) {
		t** ptr = new t * [newcap];
		for (int i = 0; i < size; i++) {
			ptr[i] = data[i];
		}
		delete[] data;
		capacity = newcap;
		data = ptr;
	}

public:

	Post(int cap) {
		if (cap <= 0) {
			capacity = 0;
		}
		else {
			capacity = cap;
		}
		data = new t * [capacity];
		size = 0;
	}

	Post(const Post& p) {
		*this = p;
	}

	//���������� �����
	void add(t* lt) {
		if (capacity == size) {
			resize(capacity * 2);
		}
		data[size++] = lt; //���������
	}

	//��������� ����� / ��������
	t* get(Str name) {
		for (int i = 0; i < size; i++) {
			//���������� ������
			if (*data[i] == name) {
				return data[i];
			}
		}
		return nullptr;
	}

	Post& operator=(const Post& p) {
		if (this != &p) {
			capacity = p.capacity;
			size = p.size;
			data = new t * [capacity];
			for (int i = 0; i < size; i++) {
				data[i] = p.data[i];
			}
		}
		return *this;
	}

	~Post() {
		delete[] data;
	}
};

int main() {
	setlocale(LC_ALL, "Russian");
	Post<Letter> p(3);
	Person p1("����");
	Letter* lt = p1.writeLetter();
	p.add(lt);
	Person p2("�������");
	Letter* data = p.get(p2.getName());
 	//if (data) {
		//if (typeid(*data).name() == typeid(Service).name()) {
		//	p2.setLetter(new Service("", ""));
		//	//����� ���������� �������� ������ Service
		//	*((Service*)p2.getLetter()) = move(*(Service*)(data));
		//}
		//else {
			p2.setLetter(new Money("", "", 0));
			//*((Money*)p2.getLetter()) = move(*(Money*)(data));
		//}
		cout << endl;
		p2.readLetter();
		p2.setLetter(data);
		cout << endl;
		p2.readLetter();
	//}
	//else {
	//	cout << "��� ������ ����������" << endl;
	//}
	cout << endl;
	return 0;
}