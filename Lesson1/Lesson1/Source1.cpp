#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include <type_traits>
#include <map>

class Human
{
public:
	Human() {}
	Human(std::string _name, int _starving) :
		name(_name), starving(_starving) {}

	void Eat(unsigned meal) 
	{
		starving -= meal;
	}
private:
	std::string name = "";
	int starving = 100; //100 - �������
};

class Employee : public Human 
{
public:
	Employee() {}
	Employee(std::string _name, int _starving,
		int _amount, unsigned _salary) :
		Human(_name, _starving), amount(_amount), 
		salary(_salary) {}

	void Siesta()
	{
		Eat(70);
	}
	void Work() 
	{
		std::cout << "I am working\n";
	}
private:
	int amount = 0;
	unsigned salary = 200;
};

class HR : public Employee
{
public:
	HR() {}
	HR(std::string _name, int _starving,
		int _amount, unsigned _salary) :
		Employee(_name, _starving, _amount, _salary) {}

	void Work() 
	{
		std::cout << "I am hiring\n";
	}
};

struct Document 
{
	void Create() 
	{
		std::cout << "Creating doc...\n";
	}
	void FillIn() 
	{
		std::cout << "Filling doc in...\n";
	}
	void Sign() 
	{
		std::cout << "Signing doc...\n";
	}
	void Save() 
	{
		std::cout << "Saving doc...\n";
	}
};

class Command
{
public:
	virtual ~Command() {}
	virtual void Execute() = 0;

protected:
	Document* doc;
	Command(Document* _doc) : doc(_doc) {}
};

struct CreateDoc : public Command 
{
	CreateDoc(Document* _doc) : Command(_doc) {}
	void Execute() 
	{
		doc->Create();
	}
};

struct FillInDoc : public Command 
{
	FillInDoc(Document* _doc) : Command(_doc) {}
	void Execute() 
	{
		doc->FillIn();
	}
};
struct SignDoc : public Command 
{
	SignDoc(Document* _doc) : Command(_doc) {}
	void Execute() 
	{
		doc->Sign();
	}
};
struct SaveDoc : public Command 
{
	SaveDoc(Document* _doc) : Command(_doc) {}
	void Execute() 
	{
		doc->Save();
	}
};

class Accountant : public Employee
{
public:
	Accountant(const std::vector<Command*>& _alg): algorithm(_alg) {}
	Accountant(std::string _name, int _starving,
		int _amount, unsigned _salary) :
		Employee(_name, _starving, _amount, _salary) {}

	void Work() 
	{
		for (auto step : algorithm)
			step->Execute();
	}
private:
	std::vector<Command*> algorithm;
};

class Developer : public Employee 
{
public:
	Developer() {}
	Developer(std::string _name, int _starving,
		int _amount, unsigned _salary) :
		Employee(_name, _starving, _amount, _salary) {}

	void Work() 
	{
		std::cout << "I am coding\n";
	}
};

class WebDeveloper : public Developer {};
class DesktopDeveloper : public Developer {};
class MobileDeveloper : public Developer {};

class FullStackDeveloper
{
public:
	void Work()
	{
		web.Work();
	}
private:
	WebDeveloper web;
	DesktopDeveloper desktop;
	MobileDeveloper mobile;
};

/*
* y = x^N
*/

//double XN(double x, int N) 
//{
//	if (N < 0)
//		return 1 / std::pow(x, -N);
//	else if (N == 0)
//		return 1;
//	else if (N % 2 == 0)
//	{
//		auto res = std::pow(x, N / 2);
//		return res * res;
//	}
//	else
//		return std::pow(x, N - 1) * x;
//}

/*
template<int N, typename T>
typename std::enable_if<(N < 0), T>::type XN(T x) { return 1 / XN<-N>(x); }

template<std::size_t N, typename T>
typename std::enable_if<(N == 0), T>::type XN(T x) { return 1; }

template<std::size_t N, typename T>
typename std::enable_if<(N == 1), T>::type XN(T x) { return x; }

template<std::size_t N, typename T>
typename std::enable_if<((N > 0) && (N % 2 == 0)), T>::type XN(T x)
{ 
	T res = XN<N / 2>(x);
	return res * res; 
}

template<std::size_t N, typename T>
typename std::enable_if<((N > 0) && (N % 2 == 1)), T>::type XN(T x)
{
	return XN<N - 1>(x) * x;
}
*/

/*
 		if (ans == '1') {
			cout << "��������� �����: ";
			getchar();
			cin >> str1;
		}
		else {
			cout << "������� ������ �������� �����: ";
			cin >> money;
			getchar();
			cout << "��������: ";
			cin >> str1;
		}
*/


class FillInStrategy 
{
public:
	virtual ~FillInStrategy() {}
	virtual void FillIn() = 0;
};

class CommonLetterFillInStrategy : public FillInStrategy
{
public:
	virtual void FillIn() 
	{
		std::cout << "CommonLetterFillInStrategy\n";
	}
};

class RegisteredLetterFillInStrategy : public FillInStrategy
{
public:
	virtual void FillIn()
	{
		std::cout << "RegisteredLetterFillInStrategy\n";
	}
};

int main()
{
	Document doc;
	std::vector<Command*> regulation;
	regulation.push_back(new CreateDoc(&doc));
	regulation.push_back(new FillInDoc(&doc));
	regulation.push_back(new SignDoc(&doc));
	regulation.push_back(new SaveDoc(&doc));

	Accountant accountant(regulation);
	accountant.Work();

	FillInStrategy* letterStrategy = new RegisteredLetterFillInStrategy();

	std::map<std::string, FillInStrategy*> strategy;
	strategy.insert({ "CommonLetter", new CommonLetterFillInStrategy() });
	strategy.insert({ "RegisteredLetter", new RegisteredLetterFillInStrategy() });

	strategy.find("CommonLetter");

	//auto y = XN<3, int>(3);

	return 0;
}