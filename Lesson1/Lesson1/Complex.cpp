/// <summary>
/// ����� ������������ �����
/// </summary>
#include <exception>
#include <iostream>
#include <concepts>
#include <cmath>

template <std::floating_point T>
class ComplexNumber
{
public:
	ComplexNumber() : _real(0), _imagine(0) {}
	ComplexNumber(T real, T imagine = 0) : _real(real), _imagine(imagine) {}

	ComplexNumber& operator=(const ComplexNumber& other);

	/// <summary>
	/// ���������� ��������� �������������� ����� �����
	/// </summary>
	/// <returns></returns>
	ComplexNumber& operator++();

	/// <summary>
	/// ����������� ��������� �������������� ����� �����
	/// </summary>
	/// <returns></returns>
	ComplexNumber operator++(int);

	/// <summary>
	/// ���������� ��������� �������������� ����� �����
	/// </summary>
	/// <returns></returns>
	ComplexNumber& operator--();

	/// <summary>
	/// ����������� ��������� �������������� ����� �����
	/// </summary>
	/// <returns></returns>
	ComplexNumber operator--(int);

	T get_real() const noexcept
	{
		return _real;
	}
	T get_imagine() const noexcept
	{
		return _imagine;
	}

	T set_real(int real) noexcept
	{
		_real = real;
	}
	T set_imagine(int imagine) noexcept
	{
		_imagine = imagine;
	}

	ComplexNumber operator+(const ComplexNumber& rhs) const noexcept
	{
		return ComplexNumber(this->_real + rhs._real, this->_imagine + rhs._imagine);
	}
	ComplexNumber operator-(const ComplexNumber& rhs) const noexcept;

	/*
		(2;0) * (3;0) = 6 + 0i
		(2;1) * (3;0) = (2*3 - 1*0) + (2*0 + 1*3)i
	*/
	ComplexNumber operator/(const ComplexNumber& rhs) const
	{
		try {
			if (this->_imagine == rhs._imagine == 0)
				throw std::exception("Zero divide");

			return ComplexNumber((this->_real * rhs._real + this->_imagine * rhs._imagine) / (rhs._real * rhs._real + rhs._imagine * rhs._imagine),
				(this->_real * rhs._imagine - this->_imagine * rhs._real) / (rhs._real * rhs._real + rhs._imagine * rhs._imagine));
		}
		catch (std::exception e)
		{
			std::cerr << e.what();
		}
	}

	ComplexNumber operator*(const ComplexNumber& rhs) const noexcept
	{
		if (this->_imagine == rhs._imagine == 0)
			return ComplexNumber(this->_real * rhs._real);

		return ComplexNumber(this->_real * rhs._real - this->_imagine * rhs._imagine,
			this->_real * rhs._imagine + this->_imagine * rhs._real);
	}
private:
	T _real;
	T _imagine;
};

template <std::floating_point T>
ComplexNumber<T>& ComplexNumber<T>::operator=(const ComplexNumber<T>& other)
{
	if (this != &other)
	{
		_real = other._real;
		_imagine = other._imagine;
	}

	return *this;
}
template <std::floating_point T>
ComplexNumber<T>& ComplexNumber<T>::operator++()
{
	++_real;
	return *this;
}

template <std::floating_point T>
ComplexNumber<T> ComplexNumber<T>::operator++(int)
{
	ComplexNumber old = *this;
	operator++();
	return old;
}

template <std::floating_point T>
ComplexNumber<T>& ComplexNumber<T>::operator--()
{
	--_real;
	return *this;
}

template <std::floating_point T>
ComplexNumber<T> ComplexNumber<T>::operator--(int)
{
	ComplexNumber<T> old = *this;
	operator--();
	return old;
}

template <std::floating_point T>
ComplexNumber<T> ComplexNumber<T>::operator-(const ComplexNumber<T>& rhs) const noexcept
{
	return ComplexNumber<T>(this->_real - rhs._real, this->_imagine - rhs._imagine);
}

int main()
{
	int a, int b;
	add<int, int>(a);
	ComplexNumber<double> a;
	ComplexNumber<double>b(1 , 1);
}