#include <iostream>
#include <string>

template <std::floating_point T>
class Human 
{
public:
	Human()
	{
		std::cout << "I am Human\n";
	}
#pragma region getters

	std::string GetName() const 
	{
		return _name;
	}
	unsigned GetAge() const
	{
		return _age;
	}
	unsigned GetStarving() const
	{
		return _starving;
	}
	unsigned GetHP() const
	{
		return _hp;
	}
	unsigned GetSanity() const
	{
		return _sanity;
	}
	float GetTemperature() const
	{
		return _temperature;
	}
	float GetAmount() const
	{
		return _amount;
	}

#pragma endregion

#pragma region setters
	void GrowUp() 
	{
		++_age;
	}
	void SetTemperature(float temperature) 
	{
		_temperature = temperature;
	}
	void SetStarving(unsigned starving)
	{
		_starving = starving;
	}
	void SetSanity(unsigned sanity)
	{
		_sanity = sanity;
	}
	void SetHP(unsigned hp)
	{
		_hp = hp;
	}
	void SetAmount(float amount)
	{
		_amount = amount;
	}

#pragma endregion

#pragma region logic

	void SetMood() 
	{
		if (_amount < 0)
			_sanity -= 50;
		else if (_amount < 500)
			_sanity -= 5;
	}

	void SelfHarm() 
	{
		if (_sanity < 10)
			_hp -= 10;
		else if (_sanity < 20)
			_hp -= 5;
	}

	void Sleep(unsigned hours) 
	{
		if (hours <= 4 && hours >= 10)
			hours = -1 * hours;

		_hp += 5 * hours;
		_sanity += 7 * hours;
	}

	void Nap(unsigned hours) 
	{
		if (hours > 3)
			Sleep(hours);
		else
		{
			_hp += 1 * hours;
			_sanity += 3 * hours;
		}
	}
#pragma endregion

private: 
#pragma region members
	std::string _name = "";
	unsigned _age = 0;
	unsigned _starving = 100; //100 - �������, 0 - ���
	unsigned _hp = 100;
	unsigned _sanity = 100;
	T _temperature = 36.6;
	T _amount = 0;

#pragma endregion
};


#pragma region Virtual_WT

template <std::floating_point T>
class WorkTime 
{
public:
	WorkTime() {}
	WorkTime(unsigned workdays) : _workdays(workdays) {}
	virtual T CalculateSalary(T salary) 
	{
		return 0;
	};
	virtual ~WorkTime() {}
	virtual WorkTime* GetCurrent() 
	{
		return this; 
	}
protected:
	unsigned _workdays = 21;
};
template <std::floating_point T>
class FullTimeJob : public WorkTime<T>
{
public:
	virtual T CalculateSalary(T salary) 
	{
		std::cout << "FullTimeJob\n";
		return WorkTime::_workdays * salary * 8;
	}
	virtual ~FullTimeJob() {}
	virtual WorkTime<T>* GetCurrent()
	{
		return this;
	}
};
template <std::floating_point T>
class ScheduledJob : public WorkTime<T>
{
public: 
	ScheduledJob() = delete;
	ScheduledJob(unsigned hours, unsigned workdays): WorkTime<T>(workdays),
		_hours(hours)  {}
	virtual T CalculateSalary(T salary)
	{
		std::cout << "ScheduledJob\n";
		return WorkTime<T>::_workdays * salary * _hours;
	}
	virtual ~ScheduledJob() {}
	virtual WorkTime<T>* GetCurrent() 
	{
		return this; 
	}
private:
	unsigned _hours = 12;
};
#pragma endregion

template <class Derived>
class Worktime
{
public:
	Worktime() {}
	Worktime(unsigned workdays) : _workdays(workdays) {}
	double CalculateSalary(double salary)
	{
		return GetCurrent()->CalculateSalaryImpl(salary);
	}
protected:
	unsigned _workdays = 21;
private:
	Derived* GetCurrent()
	{
		return static_cast<Derived*>(this);
	}
};

class FulltimeJob : public Worktime<FulltimeJob>
{
	friend class Worktime<FulltimeJob>;
private:
	double CalculateSalaryImpl(double salary)
	{
		std::cout << "FullTimeJob\n";
		return Worktime<FulltimeJob>::_workdays * salary * 8;
	}
};

class ScheduleJob : public Worktime<ScheduleJob>
{
	friend class Worktime<ScheduleJob>;
public:
	ScheduleJob() = default;
	ScheduleJob(unsigned hours, unsigned workdays) : Worktime<ScheduleJob>(workdays),
		_hours(hours) {}
private:
	double CalculateSalaryImpl(double salary)
	{
		std::cout << "ScheduledJob\n";
		return Worktime<ScheduleJob>::_workdays * salary * _hours;
	}
	unsigned _hours = 12;
};

template <std::floating_point T, class W>
class Employee : public Human<double> 
{
public:
	Employee() 
	{
		std::cout << "I am Employee\n";
	}
	void Siesta()
	{
		SetStarving(0);
		Nap(1);
	}
private:
	int salary = 188;
};

int add(int a, int b)
{
	return a + b;
}
int substract(int a, int b)
{
	return a - b;
}


int main()
{
	int (*fPtr)(int, int) = nullptr;
	fPtr = add;
	int res = fPtr(1, 3);
	fPtr = substract;

	res = fPtr(1000, 7);

	FulltimeJob j;
	auto result = j.CalculateSalary(100);
	Worktime<FulltimeJob> wj;
	result = wj.CalculateSalary(100);
	//ScheduledJob<double> j(12, 15);
	return 0;
}