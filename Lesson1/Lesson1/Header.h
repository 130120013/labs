#ifndef HEADER_H
#define HEADER_H

#include <memory>
#include <iostream>

class MyClass
{
public:
	MyClass();
	void MyMethod();
private:
	class Impl
	{
	public:
		void MyMethodImpl();
	};

	std::unique_ptr<Impl> pImpl;
};

#endif // !HEADER_H
