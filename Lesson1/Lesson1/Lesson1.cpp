﻿#include <iostream>
#include <exception>
#include <cstring>
#include <vector>
#include <random>
#include <fstream>

struct Transaction;

class Account 
{
	public:
		Account(): _amount(0)
		{
			std::random_device r;
			std::default_random_engine e1(r());
			std::uniform_int_distribution<std::size_t> uniform_dist(1, 20);
			_account_number = uniform_dist(e1);
		}
		Account(const Account& acc) = default;
		Account(std::size_t account_number, std::size_t amount): _account_number(account_number),
			_amount(amount) {}
	private:
		std::size_t _account_number;
		std::size_t _amount;
		std::vector<Transaction> _transactions;
};

struct Transaction 
{
	Account acc;
	std::size_t total;
};

class ATM
{
public:
	ATM() : _money(0), _is_working(true) {}
	ATM(const ATM& acc) = default;
	ATM(std::size_t money, bool is_working = true) : _money(money), _is_working(is_working) {}
private:
	std::size_t _money;
	bool _is_working;
};

class User 
{
	public:
		User(): _cash_amount(0) {}
		User(const Account & acc, std::size_t cash_amount): _acc(acc), _cash_amount(cash_amount) {}
		User(const Account & acc): _acc(acc), _cash_amount(0) {}
		User(const User& user): _acc(user._acc), _cash_amount(user._cash_amount) {}
	private:
		const Account _acc;
		std::size_t _cash_amount;
};

int main() 
{
	User user;
	return 0;
}