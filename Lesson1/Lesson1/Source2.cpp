#include "Header.h"

MyClass::MyClass() : pImpl(std::make_unique<Impl>()) {}

void MyClass::MyMethod()
{
	pImpl.get()->MyMethodImpl();
}
void MyClass::Impl::MyMethodImpl()
{
	std::cout << "MyMethodImpl\n";
}