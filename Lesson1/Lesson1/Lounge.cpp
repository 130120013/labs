#include <iostream>

struct CommonPass {};
struct PriorityPass {};

template <class T>
struct Lounge
{
	void Relax() 
	{
		std::cout << "Sit\n";
	}
};

template <>
struct Lounge<PriorityPass>
{
	void Relax() 
	{
		std::cout << "Eat\n";
		std::cout << "Drink\n";
		std::cout << "Sleep\n";
	}
};

int main()
{
	Lounge<int> lounge1;
	lounge1.Relax();

	Lounge<CommonPass> lounge2;
	lounge2.Relax();

	Lounge<PriorityPass> lounge3;
	lounge3.Relax();
}